# pcconvertible

A QMK keyboard config for the internal keyboard from the IBM PC Convertible (model 5140)

The IBM 5140 keyboard has a fairly unique layout, similar to modern 70% models but with a variant ANSI layout
(large enter key).

Keyboard Maintainer: [gary600](https://gitlab.com/gary600)  
Hardware Supported: Keyboard from IBM 5140, tested with Teensy++ 2.0 controller  
Hardware Availability: Somewhat hard to find

Make example for this keyboard (after setting up your build environment):

    make pcconvertible:default

See [build environment setup](https://docs.qmk.fm/build_environment_setup.html) then the [make instructions](https://docs.qmk.fm/make_instructions.html) for more information.