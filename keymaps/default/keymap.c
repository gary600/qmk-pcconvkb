/* Copyright 2018 Gary B
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

#define LAYER_DEFAULT 0
#define LAYER_FN 1
#define LAYER_WIN 2

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [LAYER_DEFAULT] = LAYOUT( // Default layout
        KC_ESC,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_INS,  KC_DEL,
        KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSLS, KC_BSPC,
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC,
        KC_CAPS, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,                   KC_ENT,
        KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, KC_PSCR,
        KC_LCTL,          KC_LALT,          KC_SPC,                                      KC_RALT,          KC_UP,
                 MO(LAYER_FN),                                                                    KC_LEFT, KC_DOWN, KC_RGHT
    ),
    [LAYER_FN] = LAYOUT( // Fn pressed
        _______, KC_MUTE, KC_VOLD, KC_VOLU, _______, _______, _______, _______, KC_MPRV, KC_MPLY, KC_MNXT, KC_NLCK, KC_PAUS, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_P7,   KC_P8,   KC_P9,   _______, KC_PMNS, KC_PPLS, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_P4,   KC_P5,   KC_P6,   _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_P1,   KC_P2,   KC_P3,   _______, _______,                   _______,
        _______, _______, _______, _______, _______, _______, _______, KC_P0,   _______, KC_PDOT, KC_PSLS, _______, KC_PAST,
        _______,          KC_LGUI,          _______,                                     _______,          KC_PGUP,
                 _______,                                                                         KC_HOME, KC_PGDN,  KC_END
    ),
	[LAYER_WIN] = LAYOUT( // Super key pressed (undo the stuff Fn did)
		_______, KC_F1,   KC_F2,   KC_F3,   _______, _______, _______, _______, _______, _______, _______, KC_F11,  KC_F12,   _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_7,    KC_8,    KC_9,    _______, KC_MINS, KC_EQL,   _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_U,    KC_I,    KC_O,    _______, _______, _______,
        _______, _______, _______, _______, _______, _______, _______, KC_J,    KC_K,    KC_L,    _______, _______,                   _______,
        _______, _______, _______, _______, _______, _______, _______, KC_M,    _______, KC_DOT,  KC_SLSH, _______, KC_PSCR,
        _______,          KC_LGUI,          _______,                                     _______,          KC_UP,
                 _______,                                                                         KC_LEFT, KC_DOWN, KC_RGHT
    ),
};

const uint16_t PROGMEM fn_actions[] = {
    //[0] = ACTION_LAYER_MOMENTARY(1)
};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    // MACRODOWN only works in this function
    switch(id) {
    case 0:
        if (record->event.pressed) {
            register_code(KC_RSFT);
        } else {
            unregister_code(KC_RSFT);
        }
        break;
    }
    return MACRO_NONE;
}


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	
	switch (keycode) {
		// Custom Win key behavior
		case KC_LGUI:
			// When pressed, select layer, and vice versa
			if (record->event.pressed) {
				layer_on(LAYER_WIN);
			} else {
				layer_off(LAYER_WIN);
			}
			// Continue processing KC_LGUI as normal
			return true;
	}
    return true;
}

void led_set_user(uint8_t usb_led) {

}
